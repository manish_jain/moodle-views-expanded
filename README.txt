INTRODUCTION
------------

This module describes various Moodle tables to Drupal's Views system, so
that site administrators can display Moodle information within a Drupal
website. The version of moodle used is 1.9.10+ (Build: 20110202).


INSTALLATION & CONFIGURATION
----------------------------

1) Install views, moodle_connector and module_views and enable it.
2) Install this module and enable it.
3) Confirm that the moodle_connector module settings are correct for your
   Moodle database. You can use a read-only database connection.
4) Optionally create new View definitions using the Moodle view types that
   this module provides.

 
CONTACT
-------

This module was written by Manish J Jain <manishjjain89@gmail.com>
