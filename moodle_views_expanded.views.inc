<?php

/**
 * @file
 * This file tells the Views module about the Moodle tables.
 * The tables are mapped from 1.9.10+ (Build: 20110202) version of moodle.
 */

/**
 * Implements hook_views_data().
 */
function moodle_views_expanded_views_data() {
    $data = array();

    // ----- adding fields for course table -----
    $data['course']['cost'] = array(
        'title' => t('Course Cost'),
        'help' => t('The cost of the course.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
            'label' => t('Course Cost')),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    $data['course']['format'] = array(
        'title' => t('Course format'),
        'help' => t('The format of the course.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'handler' => 'views_handler_filter_string',
            'label' => t('The format of the course.')),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['course']['category'] = array(
        'title' => t('Course category'),
        'help' => t('The category to which the course belongs.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Course category'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['course']['metacourse'] = array(
        'title' => t('Metacourse data'),
        'help' => t('The metacourse data for the course.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Metacourse data'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['course']['enrollable'] = array(
        'title' => t('Enrollable'),
        'help' => t('The enrollability for a course.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Enrollable'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    // adding argument handler for the course id field.
    $data['course']['id']['argument'] = array(
        'handler' => 'views_handler_argument_numeric');
    
    $data['course_categories']['id'] = array(
        'title' => t('Course category id'),
        'help' => t('The category id for a course.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Course category id'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    // ----- adding course_meta table -----
    $data['course_meta']['table']['group'] = t('Moodle Course');
    
    $data['course_meta']['child_course'] = array(
        'title' => t('Child Course ID'),
        'help' => t('The enrollability for a course.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Child Course ID'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['course_meta']['parent_course'] = array(
        'title' => t('Parent Course ID'),
        'help' => t('The enrollability for a course.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Parent Course ID'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    // ----- adding resource table -----
    $data['resource']['table']['group'] = t('Moodle Course');
    
    $data['resource']['id'] = array(
        'title' => t('Reference ID'),
        'help' => t('The unique id of the reference table.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Reference ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['resource']['name'] = array(
        'title' => t('Reference Name'),
        'help' => t('The reference name for a course.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Reference Name'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['resource']['reference'] = array(
        'title' => t('Material Reference'),
        'help' => t('Reference to the course material'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Material Reference'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['resource']['course'] = array(
        'title' => t('Material course id'),
        'help' => t('Course Id for the material'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Material course id'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    //--------------adding fields to user table-----------------------
    $data['user']['phone2'] = array(
        'title' => t('Dept ID'),
        'help' => t('The Dept ID for a user.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Dept ID'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['user']['department'] = array(
        'title' => t('Department'),
        'help' => t('The Department for the user'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Department'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    $data['user']['id']['argument'] = array(
      'handler' => 'views_handler_argument_numeric');

    // ----- adding user_info_data table -----
    $data['user_info_data']['table']['group'] = t('Moodle User');
    
    $data['user_info_data']['data'] = array(
        'title' => t('Value'),
        'help' => t('The value of the custom field for the user'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Value'),
            'handler' => 'views_handler_filter_string'),
        'argument' => array(
            'handler' => 'views_handler_argument_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['user_info_data']['fieldid'] = array(
        'title' => t('Field ID'),
        'help' => t('The ID of the field defined in the user info field table'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Field ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['user_info_data']['userid'] = array(
        'title' => t('User ID'),
        'help' => t('The user ID for whom the field date is stored.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('User ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    // ----- adding user_info_field table -----
    $data['user_info_field']['table']['group'] = t('Moodle User');

    $data['user_info_field']['shortname'] = array(
        'title' => t('Shortname'),
        'help' => t('The short name of the custom user info field'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Shortname'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    //---------------adding scorm table-----------------------
    $data['scorm']['table']['group'] = t('Moodle WBT');

    $data['scorm']['id'] = array(
        'title' => t('Scorm ID'),
        'help' => t('The unique internal ID for a scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Scorm ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['scorm']['course'] = array(
        'title' => t('Scorm Course ID'),
        'help' => t('The Scorm Course ID for a scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Scorm Course ID'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    $data['scorm']['maxgrade'] = array(
        'title' => t('Scorm Maximum Grade'),
        'help' => t('The Maximum Grade for a scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Scorm Maximum Grade'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    $data['scorm']['maxattempt'] = array(
        'title' => t('Scorm Maximum Attempts'),
        'help' => t('The Maximum no of attempts for a scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Scorm Maximum Attempts'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    $data['scorm']['reference'] = array(
    'title' => t('Scorm Reference'),
    'help' => t('The reference for a scorm package.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE),
    'filter' => array(
      'label' => t('Scorm Reference'),
      'handler' => 'views_handler_filter_string'),
    'sort' => array(
      'handler' => 'views_handler_sort'));
    
    $data['scorm']['name'] = array(
    'title' => t('Scorm Name'),
    'help' => t('The name for a scorm package.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE),
    'filter' => array(
      'label' => t('Scorm Name'),
      'handler' => 'views_handler_filter_string'),
    'sort' => array(
      'handler' => 'views_handler_sort'));
    
    $data['scorm']['summary'] = array(
    'title' => t('Scorm Summary'),
    'help' => t('The summary for a scorm package.'),
    'field' => array(
      'handler' => 'views_handler_field',
      'click sortable' => TRUE),
    'filter' => array(
      'label' => t('Scorm Summary'),
      'handler' => 'views_handler_filter_string'),
    'sort' => array(
      'handler' => 'views_handler_sort'));

    // ----- adding scorm scoes table -----
    $data['scorm_scoes']['table']['group'] = t('Moodle WBT');

    $data['scorm_scoes']['id'] = array(
        'title' => t('Scorm Scoes ID'),
        'help' => t('The Scorm Scoes ID for a scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field_scorm_scoes_id_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Scorm Scoes ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    $data['scorm_scoes']['organization'] = array(
        'title' => t('Scorm Organization'),
        'help' => t('The Scorm Organization for a scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Scorm Organization'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    // ----- adding scorm_scoes_track table -----
    $data['scorm_scoes_track']['table']['group'] = t('Moodle WBT');

    $data['scorm_scoes_track']['value'] = array(
        'title' => t('Scorm value'),
        'help' => t('Data tracked from the scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field'),
        'sort' => array(
            'handler' => 'views_handler_sort'),
        'filter' => array(
            'label' => t('Scorm value'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['scorm_scoes_track']['id'] = array(
        'title' => t('Scorm scoes ID'),
        'help' => t('Scorm Scoes ID for the scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'),
        'filter' => array(
            'label' => t('Scorm scoes ID'),
            'handler' => 'views_handler_filter_numeric'));

    $data['scorm_scoes_track']['userid'] = array(
        'title' => t('Moodle UserID'),
        'help' => t('Moodle User ID.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'filter' => array(
            'label' => t('Moodle UserID'),
            'handler' => 'views_handler_filter_numeric'));

    $data['scorm_scoes_track']['timemodified'] = array(
        'title' => t('Scorm scoes Timemodified'),
        'help' => t('Scorm scoes timemodified for the scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field_date'),
        'sort' => array(
            'handler' => 'views_handler_sort'),
        'filter' => array(
            'label' => t('Scorm scoes Timemodified'),
            'handler' => 'views_handler_filter_date'));

    $data['scorm_scoes_track']['element'] = array(
        'title' => t('Scorm scoes element'),
        'help' => t('Scorm scoes element for the scorm package.'),
        'field' => array(
            'handler' => 'views_handler_field'),
        'sort' => array(
            'handler' => 'views_handler_sort'),
        'filter' => array(
            'label' => t('Scorm scoes element'),
            'handler' => 'views_handler_filter_string'));

    // ----- adding facetoface table -----
    $data['facetoface']['table']['group'] = t('Moodle F2F Sessions');
    
    $data['facetoface']['id'] = array(
    'title' => t('Face to Face id'),
    'help' => t('The unique internal ID for a Face to face session.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric'),
    'filter' => array(
            'label' => t('Face to Face id'),
      'handler' => 'views_handler_filter_numeric'),
    'sort' => array(
      'handler' => 'views_handler_sort'));

    $data['facetoface']['course'] = array(
        'title' => t('Face to Face course No.'),
        'help' => t('The unique internal ID for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'filter' => array(
            'label' => t('Face to Face course No.'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    $data['facetoface']['showoncalendar'] = array(
        'title' => t('F2F Showoncalendar'),
        'help' => t('The showoncalendar value for F2F'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'filter' => array(
            'label' => t('F2F Showoncalendar'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface']['name'] = array(
        'title' => t('Title'),
        'help' => t('The title of the session'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Dept ID'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    // ----- adding facetoface_sessions table -----
    $data['facetoface_sessions']['table']['group'] = t('Moodle F2F Sessions');
    
    $data['facetoface_sessions']['table']['base'] = array(
        'field' => 'id',
        'title' => t('Moodle F2F Sessions'),
        'help' => t('Moodle facetoface session data.'),
        'database' => 'moodle');

    $data['facetoface_sessions']['id'] = array(
        'title' => t('Face to Face session ID'),
        'help' => t('The unique internal ID for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face session ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_sessions']['capacity'] = array(
        'title' => t('Capacity'),
        'help' => t('The number of seats available in the session.'),
        'field' => array(
            'handler' => 'views_handler_field'));

    $data['facetoface_sessions']['normalcost'] = array(
        'title' => t('Normal Cost'),
        'help' => t('The normal cost for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Normal Cost'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_sessions']['duration'] = array(
        'title' => t('Duration'),
        'help' => t('The duration of Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Duration'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_sessions']['allowoverbook'] = array(
        'title' => t('Waitlist Available'),
        'help' => t('Whether the session allows a waitlist or not'),
        'filter' => array(
            'handler' => 'views_handler_filter_boolean_operator',
            'label' => t('Waitlist is visible'),
            'type' => 'yes-no'));
    
    $data['facetoface_sessions']['course'] = array(  
    'title' => t('Face to Face course ID'),
    'help' => t('The course ID for a Face to face session.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => TRUE),
    'filter' => array(
      'label' => 'Face to Face course ID',
      'handler' => 'views_handler_filter_numeric'),
    'argument' => array(
      'handler' => 'views_handler_argument_numeric'),
    'sort' => array(
      'handler' => 'views_handler_sort'));

    // ----- adding facetoface_signups table ----- 
    $data['facetoface_signups']['table']['group'] = t('Moodle F2F Sessions');
    
    $data['facetoface_signups']['userid'] = array(
        'title' => t('Face to Face signup user ID'),
        'help' => t('The signup user ID for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face signup user ID'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_signups']['sessionid'] = array(
        'title' => t('Face to Face signup session ID'),
        'help' => t('The signup user ID for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face signup session ID'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    // ----- adding facetoface_sessions_dates table ----- 
    $data['facetoface_sessions_dates']['table']['group'] = t('Moodle F2F Sessions');

    $data['facetoface_sessions_dates']['id'] = array(
        'title' => t('Face to Face session date ID'),
        'help' => t('The session ID for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face session date ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_sessions_dates']['sessionid'] = array(
        'title' => t('Face to Face date session ID'),
        'help' => t('The session ID for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face date session date ID'),
            'handler' => 'views_handler_filter_numeric'),
        'argument' => array(
            'handler' => 'views_handler_argument_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_sessions_dates']['timestart'] = array(
        'title' => t('Session start time'),
        'help' => t('The date and time this session starts.'),
        'field' => array(
            'handler' => 'views_handler_field_date'),
        'sort' => array(
            'handler' => 'views_handler_sort_date'),
        'filter' => array(
            'label' => t('Session start time'),
            'handler' => 'views_handler_filter_date'));

    $data['facetoface_sessions_dates']['timefinish'] = array(
        'title' => t('Session end time'),
        'help' => t('The date and time this session ends.'),
        'field' => array(
            'handler' => 'views_handler_field_date'),
        'sort' => array(
            'handler' => 'views_handler_sort_date'),
        'filter' => array(
            'label' => t('Session end time'),
            'handler' => 'views_handler_filter_date'));

    // ----- adding facetoface_session_field table ----- 
    $data['facetoface_session_field']['table']['group'] = t('Moodle F2F Sessions');

    $data['facetoface_session_field']['id'] = array(
        'title' => t('Session Field ID'),
        'help' => t('The unique internal ID for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face session ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_session_field']['name'] = array(
        'title' => t('Session Name'),
        'help' => t('The Face to face session name.'),
        'field' => array(
            'handler' => 'views_handler_field',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face session ID'),
            'handler' => 'views_handler_filter_string'),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    $data['facetoface_session_data']['table']['group'] = t('Moodle F2F Sessions');
    
    $data['facetoface_session_data']['id'] = array(
        'title' => t('F2F Session data id'),
        'help' => t('The id of Face to face session data.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('F2F Session data id'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    // ----- adding facetoface_signups_status table ----- 
    $data['facetoface_signups_status']['table']['group'] = t('Moodle F2F Sessions');
    
    $data['facetoface_signups_status']['signupid'] = array(
        'title' => t('Face to Face signup ID'),
        'help' => t('The signup ID for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face signup ID'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_signups_status']['statuscode'] = array(
        'title' => t('Face to Face signup statuscode'),
        'help' => t('The signup statuscode for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face signup statuscode'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));

    $data['facetoface_signups_status']['superceded'] = array(
        'title' => t('Face to Face signup superceded'),
        'help' => t('The superceded value for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_numeric',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face signup superceded'),
            'handler' => 'views_handler_filter_numeric'),
        'sort' => array(
            'handler' => 'views_handler_sort'));


    $data['facetoface_signups_status']['timecreated'] = array(
        'title' => t('Face to Face signup timecreated'),
        'help' => t('The signup time for a Face to face session.'),
        'field' => array(
            'handler' => 'views_handler_field_date',
            'click sortable' => TRUE),
        'filter' => array(
            'label' => t('Face to Face signup timecreated'),
            'handler' => 'views_handler_filter_date'),
        'sort' => array(
            'handler' => 'views_handler_sort'));
    
    // ============ Link facetoface to facetoface_sessions ================
    $data['facetoface']['table']['join']['facetoface_sessions'] = array(
        'left_field' => 'facetoface',
        'field' => 'id');

    // ============ Link facetoface to course ================
    $data['facetoface']['table']['join']['course'] = array(
        'left_field' => 'id',
        'field' => 'course');
    
    // ============ Link facetoface_sessions to course ================
    $data['facetoface_sessions']['table']['join']['course'] = array(
        'left_table' => 'facetoface',
        'left_field' => 'id',
        'field' => 'facetoface');
    
    // ============ Link facetoface_signups to facetoface_sessions ================
    $data['facetoface_signups']['table']['join']['facetoface_sessions'] = array(
        'left_field' => 'id',
        'field' => 'sessionid');

    // ============ Link facetoface_signups to facetoface ================
    $data['facetoface_signups']['table']['join']['facetoface'] = array(
        'left_table' => 'facetoface_sessions',
        'left_field' => 'id',
        'field' => 'sessionid');

    // ============ Link facetoface_signups to course ================
    $data['facetoface_signups']['table']['join']['course'] = array(
        'left_table' => 'facetoface_sessions',
        'left_field' => 'id',
        'field' => 'sessionid');

    // ============ Link facetoface_signups_status to facetoface ================
    $data['facetoface_signups_status']['table']['join']['facetoface'] = array(
        'left_table' => 'facetoface_signups',
        'left_field' => 'id',
        'field' => 'signupid');

    // ============ Link facetoface_signups_status to facetoface_sessions ================
    $data['facetoface_signups_status']['table']['join']['facetoface_sessions'] = array(
        'left_table' => 'facetoface_signups',
        'left_field' => 'id',
        'field' => 'signupid');

    // ============ Link facetoface_signups_status to facetoface_signups ================
    $data['facetoface_signups_status']['table']['join']['facetoface_signups'] = array(
        'left_field' => 'id',
        'field' => 'signupid');

    // ============ Link facetoface_signups_status to course ================
    $data['facetoface_signups_status']['table']['join']['course'] = array(
        'left_table' => 'facetoface_signups',
        'left_field' => 'id',
        'field' => 'signupid');
    
    // ============ Link facetoface_sessions_dates to facetoface_sessions ================
    $data['facetoface_sessions_dates']['table']['join']['facetoface_sessions'] = array(
        'left_field' => 'id',
        'field' => 'sessionid');

    // ============ Link facetoface_sessions_dates to course ================
    $data['facetoface_sessions_dates']['table']['join']['course'] = array(
        'left_table' => 'facetoface_sessions',
        'left_field' => 'id',
        'field' => 'sessionid');
    
    // ============ Link facetoface_session_field to facetoface_session_data ================
    $data['facetoface_session_field']['table']['join']['facetoface_session_data'] = array(
        'left_field' => 'fieldid',
        'field' => 'id');
    
    // ============ Link facetoface_session_data to facetoface_sessions ================
    $data['facetoface_session_data']['table']['join']['facetoface_sessions'] = array(
        'left_field' => 'id',
        'field' => 'sessionid');

    // ============ Link facetoface_session_data to course ================
    $data['facetoface_session_data']['table']['join']['course'] = array(
        'left_table' => 'facetoface_sessions',
        'left_field' => 'id',
        'field' => 'sessionid');

    // ============ Link facetoface_session_field to facetoface_sessions ================
    $data['facetoface_session_field']['table']['join']['facetoface_sessions'] = array(
        'left_table' => 'facetoface_session_data',
        'left_field' => 'fieldid',
        'field' => 'id');

    // ============ Link facetoface_session_field to course ================
    $data['facetoface_session_field']['table']['join']['course'] = array(
        'left_table' => 'facetoface_session_data',
        'left_field' => 'fieldid',
        'field' => 'id');
    
    // ============ Link course_meta to facetoface_sessions ================
    $data['course_meta']['table']['join']['facetoface_sessions'] = array(
        'left_table' => 'course',
        'left_field' => 'id',
        'field' => 'child_course');
    
    // ============ Link course_meta to course ================
    $data['course_meta']['table']['join']['course'] = array(
        'left_field' => 'id',
        'field' => 'child_course');
     
    // ============ Link resource to course ================ 
    $data['resource']['table']['join']['course'] = array(
        'left_field' => 'id',
        'field' => 'course');
     
    // ============ Link scorm to user ================
    $data['scorm']['table']['join']['user'] = array(
        'left_table' => 'scorm_scoes_track',
        'left_field' => 'scormid',
        'field' => 'id');
     
    // ============ Link scorm to course ================
    $data['scorm']['table']['join']['course'] = array(
        'left_field' => 'id',
        'field' => 'course');
    
    // ============ Link scorm_scoes to scorm ================
    $data['scorm_scoes']['table']['join']['scorm'] = array(
        'left_field' => 'id',
        'field' => 'scorm');

    // ============ Link scorm_scoes to course ================
    $data['scorm_scoes']['table']['join']['course'] = array(
        'left_table' => 'scorm',
        'left_field' => 'id',
        'field' => 'scorm');
    
    // ============ Link scorm_scoes_track to scorm ================
    $data['scorm_scoes_track']['table']['join']['scorm'] = array(
        'left_field' => 'id',
        'field' => 'scormid',
    );

    // ============ Link scorm_scoes_track to course ================
    $data['scorm_scoes_track']['table']['join']['course'] = array(
            'left_table' => 'scorm',
            'left_field' => 'id',
            'field' => 'scormid');
     
    // ============ Link user_info_field to user ================
    $data['user_info_field']['table']['join']['user'] = array(
        'left_table' => 'user_info_data',
        'left_field' => 'fieldid',
        'field' => 'id');

    // ============ Link user_info_data to user ================
    $data['user_info_data']['table']['join']['user'] = array(
        'left_field' => 'id',
        'field' => 'userid');

    // ============ Link user to facetoface_sessions ================
    $data['user']['table']['join']['facetoface_sessions'] = array(
        'left_table' => 'facetoface_signups',
        'left_field' => 'userid',
        'field' => 'id');
    
    return $data;
}
